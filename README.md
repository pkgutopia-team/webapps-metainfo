= Web Application Metadata =

The purpose of this package is solely to provide metainfo files to
be processed by the AppStream metadata generator for web applications.

A web application is software (e.g. an office suite) that is accessible via
the web platform.

The AppStream metainfo file structure for the web-application metadata is documented at [1].

All metadata in this package is used by software centers such as GNOME Software and
KDE Discover to display web applications in the regular application list.
Web applications which have metadata may be launched in special web browser modes
which make them integrate better with the desktop environment, which is done
by e.g. Epiphany on the GNOME desktop.

This package must only contain references to software that has its source code
published and which has goals that align with the ideals of the Debian project.
Any non-free web applications or applications that do not align with Debians values
(e.g. if they are heavily invasive to user privacy) must not be listed here.

For those applications, a separate webapps-metainfo-nonfree source package exists.

Links:
[1]: https://www.freedesktop.org/software/appstream/docs/sect-Metadata-WebApplication.html
